<%--
/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
--%>

<%@page import="upload.multiplo.constants.UploadMultiploPortletKeys"%>
<%@page import="java.util.HashMap"%>
<%@ include file="./init.jsp" %>

<%
	long repositoryId = scopeGroupId;
	long folderId = Long.parseLong(renderRequest.getAttribute("folderId").toString());

 	Folder rootFolder = DLAppLocalServiceUtil.getFolder(folderId);
	String rootFolderName = rootFolder.getName();
	
%>

<liferay-portlet:actionURL name="/custom/document_library/upload_multiple_file_entries" var="upload_multiple_file_entriesURL">
	<portlet:param name="CMD" value="ADD_TEMP" />
	<portlet:param name="folderId" value="<%=String.valueOf(folderId)%>" />
</liferay-portlet:actionURL>

<liferay-portlet:actionURL name="/custom/document_library/delete_file_entries" var="delete_temp_folderURL">
	<portlet:param name="CMD" value="DELETE_TEMP" />
	<portlet:param name="folderId" value="<%=String.valueOf(folderId)%>" />
</liferay-portlet:actionURL>

<aui:form name="fm1">
	<div class="lfr-dynamic-uploader">
		<div class="lfr-upload-container" id="<portlet:namespace />fileUpload"></div>
	</div>
</aui:form>

<aui:script use="liferay-upload">
	new Liferay.Upload(
		{
			boundingBox: '#<portlet:namespace />fileUpload',
			allowedFileTypes: '<%= "txt" %>',
			fileDescription: '<%= StringUtil.merge(PrefsPropsUtil.getStringArray(PropsKeys.DL_FILE_EXTENSIONS, StringPool.COMMA)) %>',
			deleteFile: '<%= delete_temp_folderURL %>',
			maxFileSize: '<%= PrefsPropsUtil.getLong(PropsKeys.DL_FILE_MAX_SIZE) %> B',
			namespace: '<portlet:namespace />',
			metadataContainer: '#<portlet:namespace />commonFileMetadataContainer',
            metadataExplanationContainer: '#<portlet:namespace />metadataExplanationContainer',
			tempFileURL: {
				method: Liferay.Service.bind('/dlapp/get-temp-file-names'),
				params: {
					folderId: <%= folderId %>,
					folderName: '<%= rootFolderName %>',
					groupId: <%= scopeGroupId %>,
					tempFolderName: '<%= UploadMultiploPortletKeys.TEMP_FOLDER_NAME %>'
				}
			},
			
			<%-- tempRandomSuffix: '<%= TempFileEntryUtil.TEMP_RANDOM_SUFFIX %>', --%>
			uploadFile: '<%= upload_multiple_file_entriesURL %>'
		}
	);
</aui:script>

<!-- upload selected -->
	
<div class="common-file-metadata-container selected" id="<portlet:namespace />commonFileMetadataContainer">
	<liferay-util:include page="/upload-selected.jsp" servletContext="<%= application %>" />
</div>

<aui:script>
	Liferay.provide( window, '<portlet:namespace />updateMultipleFiles', function() {
			var A = AUI();
			var Lang = A.Lang;

			var commonFileMetadataContainer = A.one('#<portlet:namespace />commonFileMetadataContainer');
			var selectedFileNameContainer = A.one('#<portlet:namespace />selectedFileNameContainer');

			var inputTpl = '<input id="<portlet:namespace />selectedFileName{0}" name="<portlet:namespace />selectedFileName" type="hidden" value="{1}" />';

			var values = A.all('input[name=<portlet:namespace />selectUploadedFile]:checked').val();

			var buffer = [];
			var dataBuffer = [];
			var length = values.length;

			for (var i = 0; i < length; i++) {
				dataBuffer[0] = i;
				dataBuffer[1] = values[i];

				buffer[i] = Lang.sub(inputTpl, dataBuffer);
			}

			selectedFileNameContainer.html(buffer.join(''));

			commonFileMetadataContainer.plug(A.LoadingMask);

			commonFileMetadataContainer.loadingmask.show();

			A.io.request( document.<portlet:namespace />fm2.action,{
					after: {
						failure: function(event, id, obj) {
							var selectedItems = A.all('#<portlet:namespace />fileUpload li.selected');

							selectedItems.removeClass('selectable').removeClass('selected').addClass('upload-error');

							selectedItems.append('<span class="card-bottom error-message"><%= UnicodeLanguageUtil.get(request, "an-unexpected-error-occurred-while-deleting-the-file") %></span>');

							selectedItems.all('input').remove(true);

							commonFileMetadataContainer.loadingmask.hide();
						},
						success: function(event, id, obj) {
							var jsonArray = this.get('responseData');

							var itemFailed = false;

							for (var i = 0; i < jsonArray.length; i++) {
								
								var item = jsonArray[i];
								console.log('item',item);
								
							}

							commonFileMetadataContainer.loadingmask.hide();
							
						}
					},
					dataType: 'JSON',
					form: { id: document.<portlet:namespace />fm2 }
				}
			);
		},
		['aui-base']
	);
</aui:script>

