<%@ include file="./init.jsp" %>
<%

	long folderId = Long.parseLong(renderRequest.getAttribute("folderId").toString());
	List<DLFileEntryType> fileEntryTypes = DLFileEntryTypeServiceUtil.getFolderFileEntryTypes(PortalUtil.getCurrentAndAncestorSiteGroupIds(scopeGroupId), folderId, true);

%>

<portlet:actionURL name="/custom/document_library/upload_multiple_file_entries" var="uploadMultipleFileEntriesURL" />

<aui:form action="<%= uploadMultipleFileEntriesURL %>" method="post" name="fm2" onSubmit='<%= "event.preventDefault(); " + liferayPortletResponse.getNamespace() + "updateMultipleFiles();" %>'>
	<aui:input name="CMD" type="hidden" value="ADD_MULTIPLE" />
	<aui:input name="repositoryId" type="hidden" value="<%= String.valueOf(scopeGroupId) %>" />
	<aui:input name="folderId" type="hidden" value="<%= String.valueOf(folderId) %>" />

	<span id="<portlet:namespace />selectedFileNameContainer"></span>

	<aui:button type="submit" value="Publica" />
</aui:form>
