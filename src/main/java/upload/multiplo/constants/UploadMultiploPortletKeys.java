package upload.multiplo.constants;

/**
 * @author Marbola
 */
public class UploadMultiploPortletKeys {

	public static final String UPLOAD_MULTIPLO = "UploadMultiplo";
	public static final String FOLDER_NAME = "Upload Multiplo";
	public static final String TEMP_FOLDER_NAME = "temp-folder-name";
	public static final String CMD = "CMD";
	public static final String DEFAULT_ACTION = "DELETE_TEMP";
	public static final String ADD_MULTIPLE = "ADD_MULTIPLE";
	public static final String ADD_TEMP = "ADD_TEMP";
	public static final String DELETE_TEMP = "DELETE_TEMP";

}