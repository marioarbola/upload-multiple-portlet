package upload.multiplo.action;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.document.library.kernel.model.DLFileEntry;
import com.liferay.document.library.kernel.service.DLAppService;
import com.liferay.document.library.kernel.util.DLUtil;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.JSONPortletResponseUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.KeyValuePair;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.StreamUtil;
import com.liferay.portal.kernel.util.TempFileEntryUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;

import upload.multiplo.constants.UploadMultiploPortletKeys;

@Component(
    immediate = true,
    property = {
        "javax.portlet.name=" + UploadMultiploPortletKeys.UPLOAD_MULTIPLO,
        "mvc.command.name=/custom/document_library/upload_multiple_file_entries"
    },
    service = MVCActionCommand.class
)
public class UploadActionUrl extends BaseMVCActionCommand {

	private static Log _log = LogFactoryUtil.getLog(UploadActionUrl.class);

	@Reference
	private Portal _portal;
	
	@Reference(unbind = "-")
	private DLAppService _documentlibraryService;
		
	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		
		String action = ParamUtil.getString(actionRequest, UploadMultiploPortletKeys.CMD , UploadMultiploPortletKeys.DEFAULT_ACTION);
		PortletConfig portletConfig = getPortletConfig(actionRequest);
		
		switch (action) {
			case UploadMultiploPortletKeys.ADD_MULTIPLE:{
				addMultipleFileEntries(portletConfig, actionRequest, actionResponse);
				break;
			}
			case UploadMultiploPortletKeys.ADD_TEMP:{
				addMultipleFileEntries(portletConfig, actionRequest, actionResponse);
				break;
			}
			case UploadMultiploPortletKeys.DELETE_TEMP:{
				addMultipleFileEntries(portletConfig, actionRequest, actionResponse);
				break;
			}
			default:{
				break;
			}
		}
		
	}

	private void addMultipleFileEntries(PortletConfig portletConfig, ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {

		List<KeyValuePair> validFileName = new ArrayList<>();
		List<KeyValuePair> invalidFileName = new ArrayList<>();

		String[] selectedFileNames = ParamUtil.getParameterValues(actionRequest, "selectedFileName", new String[0], false);

		for (String selectedFileName : selectedFileNames) {
			addFileEntries(portletConfig, actionRequest, actionResponse, selectedFileName,validFileName, invalidFileName);
		}

		JSONArray jsonArray = JSONFactoryUtil.createJSONArray();

		for (KeyValuePair validFileNameKVP : validFileName) {
			String fileName = validFileNameKVP.getKey();
			String originalFileName = validFileNameKVP.getValue();

			JSONObject jsonObject = JSONFactoryUtil.createJSONObject();

			jsonObject.put("added", Boolean.TRUE);
			jsonObject.put("fileName", fileName);
			jsonObject.put("originalFileName", originalFileName);

			jsonArray.put(jsonObject);
		}

		for (KeyValuePair invalidFileNameKVP : invalidFileName) {
			String fileName = invalidFileNameKVP.getKey();
			String errorMessage = invalidFileNameKVP.getValue();

			JSONObject jsonObject = JSONFactoryUtil.createJSONObject();

			jsonObject.put("added", Boolean.FALSE);
			jsonObject.put("errorMessage", errorMessage);
			jsonObject.put("fileName", fileName);
			jsonObject.put("originalFileName", fileName);

			jsonArray.put(jsonObject);
		}

		JSONPortletResponseUtil.writeJSON(actionRequest, actionResponse, jsonArray);
	}
	
	private void addFileEntries(PortletConfig portletConfig, ActionRequest actionRequest, ActionResponse actionResponse,
			String selectedFileName, List<KeyValuePair> validFileNameKVPs, List<KeyValuePair> invalidFileNameKVPs)
			throws Exception {

		ThemeDisplay themeDisplay = (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);

		long repositoryId = ParamUtil.getLong(actionRequest, "repositoryId");
		long folderId = ParamUtil.getLong(actionRequest, "folderId");
		String description = ParamUtil.getString(actionRequest, "description");
		String changeLog = ParamUtil.getString(actionRequest, "changeLog");

		FileEntry tempFileEntry = null;

		try {
			tempFileEntry = TempFileEntryUtil.getTempFileEntry(themeDisplay.getScopeGroupId(), themeDisplay.getUserId(),
					UploadMultiploPortletKeys.TEMP_FOLDER_NAME, selectedFileName);

			String originalSelectedFileName = TempFileEntryUtil.getOriginalTempFileName(tempFileEntry.getFileName());

			String uniqueFileName = DLUtil.getUniqueFileName(tempFileEntry.getGroupId(), folderId, originalSelectedFileName);

			String mimeType = tempFileEntry.getMimeType();
			InputStream inputStream = tempFileEntry.getContentStream();
			long size = tempFileEntry.getSize();

			ServiceContext serviceContext = ServiceContextFactory.getInstance(DLFileEntry.class.getName(),actionRequest);

			_documentlibraryService.addFileEntry(repositoryId, folderId, uniqueFileName, mimeType, uniqueFileName, description,
					changeLog, inputStream, size, serviceContext);

			validFileNameKVPs.add(new KeyValuePair(uniqueFileName, selectedFileName));

			return;
			
		} catch (Exception e) {
			_log.error(e);
			invalidFileNameKVPs.add(new KeyValuePair(selectedFileName, e.getMessage()));
			
		} finally {
			
			if (Validator.isNotNull(tempFileEntry)) {
				TempFileEntryUtil.deleteTempFileEntry(tempFileEntry.getFileEntryId());
			}
			
		}
	}
	
	protected void addTempFileEntry(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {

		UploadPortletRequest uploadPortletRequest = _portal.getUploadPortletRequest(actionRequest);

		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);

		long folderId = ParamUtil.getLong(uploadPortletRequest, "folderId");
		String sourceFileName = uploadPortletRequest.getFileName("file");

		InputStream inputStream = null;

		try {
			String tempFileName = TempFileEntryUtil.getTempFileName(sourceFileName);

			inputStream = uploadPortletRequest.getFileAsStream("file");

			String mimeType = uploadPortletRequest.getContentType("file");

			FileEntry fileEntry = _documentlibraryService.addTempFileEntry(themeDisplay.getScopeGroupId(), folderId,
					UploadMultiploPortletKeys.TEMP_FOLDER_NAME, tempFileName, inputStream, mimeType);

			JSONObject jsonObject = JSONFactoryUtil.createJSONObject();

			jsonObject.put("groupId", fileEntry.getGroupId());
			jsonObject.put("name", fileEntry.getTitle());
			jsonObject.put("title", sourceFileName);
			jsonObject.put("uuid", fileEntry.getUuid());

			JSONPortletResponseUtil.writeJSON(actionRequest, actionResponse, jsonObject);
		} finally {
			StreamUtil.cleanUp(inputStream);
		}
	}

	protected void deleteTempFileEntry(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {

		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);

		long folderId = ParamUtil.getLong(actionRequest, "folderId");
		String fileName = ParamUtil.getString(actionRequest, "fileName");

		JSONObject jsonObject = JSONFactoryUtil.createJSONObject();

		try {
			_documentlibraryService.deleteTempFileEntry(themeDisplay.getScopeGroupId(), folderId, UploadMultiploPortletKeys.TEMP_FOLDER_NAME, fileName);

			jsonObject.put("deleted", Boolean.TRUE);
		} catch (Exception e) {
			String errorMessage = themeDisplay.translate("an-unexpected-error-occurred-while-deleting-the-file");

			jsonObject.put("deleted", Boolean.FALSE);
			jsonObject.put("errorMessage", errorMessage);
		}

		JSONPortletResponseUtil.writeJSON(actionRequest, actionResponse, jsonObject);
	}
}


